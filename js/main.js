document.getElementById("hello_text").textContent = "はじめてのJavaScript";


let count = 0

let cell_array = []

let blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
};


loadTable();
setInterval(function () {
        count++

        document.getElementById("hello_text").textContent = "初めてのJavaScript(" + count + ")"

        fallBlocks();


    },
    1000
)


function loadTable() {
    cell_array = []
    let td_array = document.getElementsByTagName("td")
    let index = 0

    for (let row = 0; row < 20; row++) {
        cell_array[row] = [];
        for (let col = 0; col < 10; col++) {
            cell_array[row][col] = td_array[index]
            index++
        }
    }
}

function fallBlocks() {

    // 最下層のクラスを空にする
    for (let i = 0; i < 10; i++) {
        if (cell_array[19][i].className !== "") {
            isFalling = false
            return
        }

    }
    // クラスを下げていく
    for (let row = 18; row >= 0; row--) {
        for (let col = 0; col < 10; col++) {
            if (cell_array[row][col].blockNum === fallingBlockNum) {
                if (cell_array[row + 1][col].className !== "" && cell_array[row + 1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    return; // 一つ下のマスにブロックがいるので落とさない
                }
            }
        }
    }
    // 下から二番目の行から繰り返しクラスを下げていく
    for (let row = 18; row >= 0; row--) {
        for (let col = 0; col < 10; col++) {
            if (cell_array[row][col].blockNum === fallingBlockNum) {
                cell_array[row + 1][col].className = cell_array[row][col].className;
                cell_array[row + 1][col].blockNum = cell_array[row][col].blockNum;
                cell_array[row][col].className = "";
                cell_array[row][col].blockNum = null;
            }
        }
    }
}

let isFalling = true;

function hasFallingBlock() {
    return isFalling
}

function deleteRow() {

}

let fallingBlockNum = 0

function generateBlock() {
    let keys = Object.keys(blocks)
    let nextBlockKey = keys[Math.floor(Math.random() * keys.length)]
    let nextBlock = blocks[nextBlockKey]
    let nextFallingBlockNum = fallingBlockNum + 1;

    let pattern = nextBlock.pattern
    for (let row = 0; row < pattern.length; row++) {
        for (let col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cell_array[row][col + 3].className = nextBlock.class
                cell_array[row][col + 3].blockNum = nextFallingBlockNum
            }
        }
    }

    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;

}

function moveRight() {

}

function moveLeft() {

}